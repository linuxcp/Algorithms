package com.william.sort;

public class BubbleSort {

	public static int[] sort(int[] args){
		if(null == args || 0 == args.length){
			return null;
		}
		if(args.length < 2){
			return args;
		}
		for(int i = 1; i < args.length; i++){
			for(int j = 1; j < args.length; j++){
				if(args[j] < args[j-1]){
					int temp = args[j];
					args[j] = args[j-1];
					args[j-1] = temp;
				}
			}
		}
		return args;
	}
	
	public static void print(int[] data){
		if(null == data || 0 == data.length){
			return;
		}
		for(int i:data){
			System.out.print(i+" ");
		}
	}	
	public static void main(String[] args) {
		// TODO Auto-gen1erated method stub
		int[] data = new int[]{3,2,6,1,3,4,32,62,0,2,4,65,2,5};
		BubbleSort.print(data);
		System.out.println();
		BubbleSort.print(BubbleSort.sort(data));
	}

}
