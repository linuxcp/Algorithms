package com.william.sort;

public class InsertSort {

	public static int[] sort(int[] data){
		if(null == data || 0 == data.length){
			return null;
		}
		if(data.length < 2){
			return data;
		}
		for(int i=0; i<data.length; i++){
			int temp = i;
			for(int j = i; j < data.length; j++){
				if(data[j] < data[temp]){
					temp = j;
				}
			}
			int tempData = data[temp];
			data[temp] = data[i];
			data[i] = tempData;
		}
		return data;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] data = new int[]{3,2,6,1,3,4,32,62,0,2,4,65,2,5};
		BubbleSort.print(data);
		System.out.println();
		BubbleSort.print(InsertSort.sort(data));
	}

}
