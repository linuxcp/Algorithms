package com.william.sort;

public class ShellSort {

	public static int[] sort(int[] data){
		for(int group = data.length/2; group > 0; group /=2){
			for(int i = group; i < data.length; i++){
				for(int j = i - group; j >= 0; j -= group){
					if(data[j] > data[j + group]){
						int temp = data[j];
						data[j] = data[j+group];
						data[j+group] = temp;
					}
				}
			}
		}
		return data;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] data = new int[]{3,2,6,1,3,4,32,62,0,2,4,65,2,5};
		BubbleSort.print(data);
		System.out.println();
		BubbleSort.print(ShellSort.sort(data));
	}

}
