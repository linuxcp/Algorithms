package com.william.find;

public class BinarySearch {

	public static int find(int[] data, int key){
		if(null == data || 0 == data.length){
			return -1;
		}
		if(data.length == 1){
			return 0;
		}
		int low = 0;
		int high = data.length;
		while(low <= high){
			int mid = (low + high) / 2;
			if(data[mid] > key) high = mid -1;
			else if(data[mid] < key) low = mid+1;
			else return mid;
		}
		return -1;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] data = new int[]{0 , 2 ,2, 2, 3, 3, 4, 4, 5, 6, 32, 62, 65};
		System.out.print(BinarySearch.find(data, 65));
	}

}
